package com.cpe.groupe3.usermicroservice.controller;

import java.util.List;

// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cpe.groupe3.usermicroservice.service.DispatcherUserService;
import com.cpe.groupe3.usermicroservicepublic.dto.UserDTO;
import com.cpe.groupe3.usermicroservicepublic.dto.UserLoginDTO;
import com.cpe.groupe3.usermicroservicepublic.dto.UserTransactionDTO;
import com.cpe.groupe3.usermicroservicepublic.rest.IUserRest;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class UserRestController implements IUserRest {
	// private final static Logger LOG = LoggerFactory.getLogger(UserRestController.class);
	@Autowired
	private DispatcherUserService dispatcherUserService;
	
	public UserRestController() {
	}

	@Override
	@SecurityRequirement(name = "Bearer Authentication")
	public List<UserDTO> getAll() {
		return dispatcherUserService.getAllUsers();
	}

	@Override
	public UserDTO get(@PathVariable String id) {
		return dispatcherUserService.getUser(id);
	}


	@Override
	public UserDTO add(@RequestBody UserDTO user) {
		return dispatcherUserService.addUser(user);
	}

	@Override
	public UserDTO update(@RequestBody UserDTO user, @PathVariable String id) {
		user.setId(Integer.valueOf(id));
		return dispatcherUserService.updateUser(user);
	}
	
	@Override
	public void updateForTransaction(@RequestBody UserTransactionDTO user, @PathVariable Integer id) {
		user.getUser().setId(id);
		dispatcherUserService.updateUserForTransaction(user);
	}

	@Override
	public void delete(@PathVariable String id) {
		dispatcherUserService.deleteUser(id);
	}

	@Override
	public UserDTO getByLogin(@RequestBody UserLoginDTO uLD) {
		return dispatcherUserService.getUserByLogin(uLD);
	}
}
