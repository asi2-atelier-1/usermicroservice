package com.cpe.groupe3.usermicroservice.exception;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.cpe.groupe3.commonmicroservice.dto.ErrorDTO;

@RestControllerAdvice
@Order(Ordered.LOWEST_PRECEDENCE)
public class UserExceptionHandler {
    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<ErrorDTO> handleUserNotFoundException(final UserNotFoundException ex){
        return new ResponseEntity<ErrorDTO>(new ErrorDTO("Invalid User", ex.getMessage(), HttpStatus.NOT_FOUND.value(), UserNotFoundException.class.getSimpleName()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(UserAlreadyExistsException.class)
    public ResponseEntity<ErrorDTO> handleUserAlreadyExistsException(final UserAlreadyExistsException ex){
        return new ResponseEntity<ErrorDTO>(new ErrorDTO("Invalid Request", ex.getMessage(), HttpStatus.BAD_REQUEST.value(), UserAlreadyExistsException.class.getSimpleName()), HttpStatus.BAD_REQUEST);
    }
}
