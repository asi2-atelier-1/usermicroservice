package com.cpe.groupe3.usermicroservice.service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cpe.groupe3.commonmicroservice.service.QueueService;
import com.cpe.groupe3.transactionmicroservicepublic.consumer.TransactionRestConsumer;
import com.cpe.groupe3.transactionmicroservicepublic.dto.UpdateTransactionDTO;
import com.cpe.groupe3.usermicroservice.exception.UserAlreadyExistsException;
import com.cpe.groupe3.usermicroservice.exception.UserNotFoundException;
import com.cpe.groupe3.usermicroservice.mapper.UserMapper;
import com.cpe.groupe3.usermicroservice.model.UserModel;
import com.cpe.groupe3.usermicroservice.repository.UserRepository;
import com.cpe.groupe3.usermicroservicepublic.dto.UserDTO;
import com.cpe.groupe3.usermicroservicepublic.dto.UserLoginDTO;
import com.cpe.groupe3.usermicroservicepublic.dto.UserTransactionDTO;

@Service
public class UserService extends QueueService implements IUserService {
	@Autowired
	private TransactionRestConsumer transactionRestConsumer;
	@Autowired
	private UserRepository userRepository;
	
	public UserService() {
	}

	public List<UserDTO> getAllUsers() {
		List<UserDTO> userList = new ArrayList<>();
		for (UserModel uM:userRepository.findAll()){
			userList.add(UserMapper.fromUserModelToUserDTO(uM));
		}
		return userList;
	}

	public UserDTO getUser(String id){
		Optional<UserModel> uM = userRepository.findById(Integer.valueOf(id));
		if(uM.isPresent()) {
			return UserMapper.fromUserModelToUserDTO(uM.get());
		}
		throw new UserNotFoundException("User does not exist");
	}

	private boolean doesUserEmailExists(String email) {
		try {
			userRepository.findByEmail(email).get();
			return true;	
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public UserDTO addUser(UserDTO user) {
		if (doesUserEmailExists(user.getEmail())){
			throw new UserAlreadyExistsException("User with mail " + user.getEmail() + " already exists");
		}
		UserModel u = UserMapper.fromDtoToModel(user);
		UserModel uBd = userRepository.save(u);
		return UserMapper.fromUserModelToUserDTO(uBd);
	}

	public UserDTO updateUser(UserDTO user) {
		getUser(user.getId().toString()); // Checking the user exists before updating it
		UserModel uBd = userRepository.save(UserMapper.fromDtoToModel(user));
		return UserMapper.fromUserModelToUserDTO(uBd);
	}
	
	/*
	 * It does not matter if the user doesn't exist, it'll set the userOk field to false and rollback the transaction
	 */
	public void updateUserForTransaction(UserTransactionDTO userTD) {
		UpdateTransactionDTO uTD = new UpdateTransactionDTO(userTD.getOrderId());
		try{
			updateUser(userTD.getUser());
			uTD.setUserOk(true);
		}catch (Exception e){
			uTD.setUserOk(false);
		}finally{
			transactionRestConsumer.updateTransaction(uTD, userTD.getOrderId());
		}
	}

	public void deleteUser(String id) {
		userRepository.deleteById(Integer.valueOf(id));
	}

	public UserDTO getUserByLogin(UserLoginDTO uLD) {
		Optional<UserModel> uM = userRepository.findByEmailAndPwd(uLD.getEmail(), uLD.getPwd());
		if(uM.isPresent()) {
			return UserMapper.fromUserModelToUserDTO(uM.get());
		}
		throw new UserNotFoundException("User with mail " + uLD.getEmail() + " does not exist");
	}
}