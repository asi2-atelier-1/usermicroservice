package com.cpe.groupe3.usermicroservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cpe.groupe3.cardmicroservicepublic.consumer.CardInstanceRestConsumer;
import com.cpe.groupe3.commonmicroservice.message.QueueMessage;
import com.cpe.groupe3.usermicroservicepublic.dto.UserDTO;
import com.cpe.groupe3.usermicroservicepublic.dto.UserLoginDTO;
import com.cpe.groupe3.usermicroservicepublic.dto.UserTransactionDTO;

@Component
public class DispatcherUserService implements IUserService {
	@Autowired
	private UserService userService;
	@Autowired
	private CardInstanceRestConsumer cardInstanceRestConsumer;
	
	public List<UserDTO> getAllUsers() {
		return userService.getAllUsers();
	}

	public UserDTO getUser(String id) {
		return userService.getUser(id);
	}

	public UserDTO addUser(UserDTO user) {
		UserDTO userAdded = userService.addUser(user);
		cardInstanceRestConsumer.addCardInstancesToNewUser(userAdded.getId());
		return userAdded;
	}

	public UserDTO updateUser(UserDTO user) {
		return userService.updateUser(user);
	}

	public void updateUserForTransaction(UserTransactionDTO user) {
		userService.post(new QueueMessage<UserTransactionDTO>(user, "updateUserForTransaction"));
	}
	
	public void deleteUser(String id) {
		userService.deleteUser(id);
	}

	public UserDTO getUserByLogin(UserLoginDTO login) {
		return userService.getUserByLogin(login);
	}

}
