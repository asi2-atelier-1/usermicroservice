package com.cpe.groupe3.usermicroservice.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.cpe.groupe3.usermicroservice.model.UserModel;

@Repository
public interface UserRepository extends CrudRepository<UserModel, Integer> {
	public Optional<UserModel> findByEmailAndPwd(String email, String pwd);
	public Optional<UserModel> findByEmail(String email);
}
