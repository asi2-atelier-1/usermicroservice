package com.cpe.groupe3.usermicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@SpringBootApplication
@ComponentScan(basePackages = { "com.cpe.groupe3.authmicroservicepublic", "com.cpe.groupe3.commonmicroservice", "com.cpe.groupe3.cardmicroservicepublic", "com.cpe.groupe3.transactionmicroservicepublic" })
@ComponentScan
@OpenAPIDefinition(info = @Info(title = "User MicroService Rest Api", version = "1.0", description = "Information about the User MicroService APi and how to interact with"))
public class UserMicroService {
	public static void main(String[] args) {
		SpringApplication.run(UserMicroService.class, args);
	}
}

