package com.cpe.groupe3.usermicroservice.mapper;

import com.cpe.groupe3.usermicroservice.model.UserModel;
import com.cpe.groupe3.usermicroservicepublic.dto.UserDTO;

public class UserMapper {
	public static UserDTO fromUserModelToUserDTO(UserModel uM) {
		UserDTO uDto = new UserDTO(uM.getId(), uM.getLogin(), uM.getPwd(), uM.getAccount(), uM.getLastName(), uM.getSurName(), uM.getEmail());
		return uDto;
	}
	
	public static UserModel fromDtoToModel(UserDTO uD) {
		UserModel uM = new UserModel();
		uM.setAccount(uD.getAccount());
		uM.setEmail(uD.getEmail());
		uM.setId(uD.getId());
		uM.setLastName(uD.getLastName());
		uM.setLogin(uD.getLogin());
		uM.setPwd(uD.getPwd());
		uM.setSurName(uD.getSurName());
		return uM;
	}

}
