package com.cpe.groupe3.usermicroservice.service;

import java.util.List;

import com.cpe.groupe3.usermicroservicepublic.dto.UserDTO;
import com.cpe.groupe3.usermicroservicepublic.dto.UserLoginDTO;
import com.cpe.groupe3.usermicroservicepublic.dto.UserTransactionDTO;

public interface IUserService {
	public List<UserDTO> getAllUsers();
	public UserDTO getUser(String id);
	public UserDTO addUser(UserDTO user);
	public UserDTO updateUser(UserDTO user);
	public void updateUserForTransaction(UserTransactionDTO user);
	public void deleteUser(String id);
	public UserDTO getUserByLogin(UserLoginDTO login);
}
