package com.cpe.groupe3.usermicroservicepublic.rest;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cpe.groupe3.usermicroservicepublic.dto.UserDTO;
import com.cpe.groupe3.usermicroservicepublic.dto.UserLoginDTO;
import com.cpe.groupe3.usermicroservicepublic.dto.UserTransactionDTO;

public interface IUserRest {
    public final String ROOT_PATH = "/users";

    public final String ALL = ROOT_PATH;
    public final String GET = ROOT_PATH + "/{id}";
    public final String ADD = ROOT_PATH;
    public final String UPDATE = ROOT_PATH + "/{id}";
    public final String UPDATE_FOR_TRANSACTION = ROOT_PATH + "/transaction/{id}";
    public final String DELETE = ROOT_PATH + "/{id}";
    public final String LOGIN = ROOT_PATH + "/login";

    @RequestMapping(method = RequestMethod.GET, value = ALL)
    public List<UserDTO> getAll();

    @RequestMapping(method = RequestMethod.GET, value = GET)
    public UserDTO get(String id);

    @RequestMapping(method = RequestMethod.POST, value = ADD)
    public UserDTO add(UserDTO user);
    
    @RequestMapping(method = RequestMethod.PATCH, value = UPDATE)
    public UserDTO update(UserDTO user, String id);
    
    @RequestMapping(method = RequestMethod.PATCH, value = UPDATE_FOR_TRANSACTION)
    public void updateForTransaction(UserTransactionDTO user, Integer id);
    
    @RequestMapping(method = RequestMethod.DELETE, value = DELETE)
    public void delete(String id);

    @RequestMapping(method = RequestMethod.POST, value = LOGIN)
    public UserDTO getByLogin(UserLoginDTO uLD);
}
