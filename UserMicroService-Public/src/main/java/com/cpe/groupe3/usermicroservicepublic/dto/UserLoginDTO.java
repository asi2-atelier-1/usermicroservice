package com.cpe.groupe3.usermicroservicepublic.dto;

public class UserLoginDTO {
    private String email;
	private String pwd;

    public UserLoginDTO() {
		
	}
	
	public UserLoginDTO(String email, String pwd) {
		this.email = email;
		this.pwd = pwd;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
}
