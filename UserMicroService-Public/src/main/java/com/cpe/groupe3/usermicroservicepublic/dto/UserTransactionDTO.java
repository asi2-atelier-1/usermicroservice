package com.cpe.groupe3.usermicroservicepublic.dto;

import java.io.Serializable;

public class UserTransactionDTO implements Serializable{
	private static final long serialVersionUID = -2711930141719623471L;
	private UserDTO user;
	private Integer orderId;
	
	public UserTransactionDTO() {	
	}

	public UserTransactionDTO(UserDTO user, Integer orderId) {
		this.user = user;
		this.orderId = orderId;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
}
