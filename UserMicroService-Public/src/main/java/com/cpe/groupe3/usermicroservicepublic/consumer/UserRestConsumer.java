package com.cpe.groupe3.usermicroservicepublic.consumer;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

import com.cpe.groupe3.commonmicroservice.consumer.RestConsumer;
import com.cpe.groupe3.usermicroservicepublic.dto.UserDTO;
import com.cpe.groupe3.usermicroservicepublic.dto.UserLoginDTO;
import com.cpe.groupe3.usermicroservicepublic.dto.UserTransactionDTO;
import com.cpe.groupe3.usermicroservicepublic.rest.IUserRest;

@Component("userRestConsumer")
public class UserRestConsumer extends RestConsumer implements IUserRest {
    public UserRestConsumer() {
        super("8081");
    }

    @Override
    public List<UserDTO> getAll() {
        return Arrays.asList(getForEntity(ALL, UserDTO[].class));
    }

    @Override
    public UserDTO get(String id) {
        return getForEntity(GET, UserDTO.class, id);
    }

    @Override
    public UserDTO add(UserDTO user) {
        return postForEntity(ADD, user, UserDTO.class);
    }

    @Override
    public UserDTO update(UserDTO user, String id) {
        return patchForEntity(UPDATE, user, UserDTO.class, id);
    }

    @Override
    public void updateForTransaction(UserTransactionDTO user, Integer id) {
        patchForEntity(UPDATE_FOR_TRANSACTION, user, UserTransactionDTO.class, id);
    }
    
    @Override
    public void delete(String id) {
        delete(id);
    }

    @Override
    public UserDTO getByLogin(UserLoginDTO uLD) {
        return postForEntity(LOGIN, uLD, UserDTO.class);
    }
}
